const msgerForm = $(".msger-inputarea");
const msgerInput = $(".msger-input");
const msgerChat = $(".msger-chat");
const msgerBotTyping = $(".msger-bot-typing");
const sourceBtn = $(".source-btn");
const msgerSendBtn = $(".msger-send-btn");

// Icons made by Freepik from www.flaticon.com
const BOT_IMG = "https://image.flaticon.com/icons/svg/327/327779.svg";
const PERSON_IMG = "https://image.flaticon.com/icons/svg/145/145867.svg";
const BOT_NAME = "ChatBot";
const PERSON_NAME = "Your";

msgerBotTyping.css("display", "none");

appendMessage(BOT_NAME, BOT_IMG, "left", "Hi, welcome to ChatBot! Go ahead and send me a message. 😄");

msgerForm.submit(function(event) {
	event.preventDefault();

	const msgText = msgerInput.val();
	if (!msgText) return;

	appendMessage(PERSON_NAME, PERSON_IMG, "right", msgText);
	msgerInput.val("");

	if(msgerBotTyping.css("display") === "none") {
		msgerBotTyping.css("display", "inline-block");
	} else {
		msgerBotTyping.css("display", "none");
	}

	getBotResponse(msgText);
});

function appendMessage(name, img, side, text) {
	//   Simple solution for small apps
	const msgHTML = `
    <div class="msg ${side}-msg">
      <div class="msg-img" style="background-image: url(${img})"></div>

      <div class="msg-bubble">
	<div class="msg-info">
	  <div class="msg-info-name">${name}</div>
	  <div class="msg-info-time">${formatDate(new Date())}</div>
	</div>

	<div class="msg-text">${text}</div>
      </div>
    </div>
  `;
	msgerChat.append(msgHTML);
	msgerChat.animate({
		scrollTop : msgerChat.prop("scrollHeight")
	}, 1000);
}

function getBotResponse(userMessage) {
	const msgTex = "";
	$.get("/chatbot?message=" + userMessage, function(data, status) {
		appendMessage(BOT_NAME, BOT_IMG, "left", data.message);
		if(msgerBotTyping.css("display") === "none") {
			msgerBotTyping.css("display", "inline-block");
		} else {
			msgerBotTyping.css("display", "none");
		}
	});
}

// Utils
function formatDate(date) {
	const h = "0" + date.getHours();
	const m = "0" + date.getMinutes();

	return `${h.slice(-2)}:${m.slice(-2)}`;
}
