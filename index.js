const Discord = require ('discord.js')
const bot = require ('cleverbot-free')
const express = require ('express')
const geoip = require('geoip-lite')
const MongoClient = require('mongodb').MongoClient
const app = express()
const client = new Discord.Client()
const env = process.env

const uri = `mongodb+srv://${env.DB_USERNAME}:${env.DB_PASSWORD}@${env.DB_CLUSTER_URL}/${env.DB_NAME}?retryWrites=true&w=majority`

app.set('trust proxy', true)

app.use(express.static("/"))

app.use(express.static("public"))

app.get('/', async function(request, response) {

	response.status(200)
	response.header("Content-Type", 'text/html')
	response.sendFile(__dirname + '/views/index.html')

	var geo = geoip.lookup(request.ip)
	
	const dbClient = new MongoClient(uri, { useUnifiedTopology: true })

	try {

	await dbClient.connect()
	const database = dbClient.db(env.DB_NAME)
	const collection = database.collection('Log')

	const result = await collection.insertOne({Headers: JSON.stringify(request.headers),
		IP: JSON.stringify(request.ip),
		Browser: request.headers["user-agent"],
		Language: request.headers["accept-language"],
		Country: (geo ? geo.country: "Unknown"),
		Region: (geo ? geo.region: "Unknown"),
		Logs: geo})

	console.log(`${result.insertedCount} documents were inserted with the _id: ${result.insertedId}`)

	} catch(e) {

		console.error(e)

	} finally {

		await dbClient.close()

	}

})

app.get('/chatbot', async function(request, response) {

	const message = request.query.message

	bot(message).then(bot_response => {

		response.status(200)
		response.header("Content-Type",'application/json')
		response.end(JSON.stringify({message : bot_response}))

	})

	var geo =geoip.lookup(request.ip)

	const dbClient = new MongoClient(uri, { useUnifiedTopology: true })

	try {

		await dbClient.connect()
		const database = dbClient.db(env.DB_NAME)
		const collection = database.collection('Log')

		const result = await collection.insertOne({Headers: JSON.stringify(request.headers),
			IP: JSON.stringify(request.ip),
			Browser: request.headers["user-agent"],
			Language: request.headers["accept-language"],
			Country: (geo ? geo.country: "Unknown"),
			Region: (geo ? geo.region: "Unknown"),
			Logs: geo})

		console.log(`${result.insertedCount} documents were inserted with the _id: ${result.insertedId}`)

	} catch(e) {

		console.error(e)

	} finally {

		await dbClient.close()

	}

})

app.listen(env.PORT || 3000, function() {

	console.log('Your app is listening on port 3000')

})

client.on('ready', function() {

	console.log(`Logged in as ${client.user.tag}!`)

})

client.on('message', msg => {

	if (msg.author.bot) return;

	if (msg.channel.type === "dm")
		console.log(msg.content)
	return bot(msg.content).then(bot_response => {

		msg.channel.startTyping()
		setTimeout(() => {

			msg.reply(bot_response)
			msg.channel.stopTyping()

		}, bot_response.length * (1-3) + 1 * 1000)

	})

})

client.login(env.DISCORD_TOKEN)


